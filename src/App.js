import "./App.css";
import MyCounterItem from "./components/counter/MyCounterItem";
import Cart from "./components/cart/Cart";
import GuessGame from "./components/guessGame/GuessGame";

const App = () => {
  const counters = [
    { id: 1, initial: 6, min: -5, max: 10 },
    { id: 2, initial: 5 },
    { id: 3 }
  ];

  return (
    <div className="App">
      <header className="App-header">
        {counters.map(counter => <MyCounterItem
          key={counter.id}
          initialValue={counter.hasOwnProperty("initial") ? counter.initial : undefined}
          maxValue={counter.hasOwnProperty("max") ? counter.max : undefined}
          minValue={counter.hasOwnProperty("min") ? counter.min : undefined}
        />)}
        <Cart/>
        <GuessGame/>
      </header>
    </div>
  );
};

export default App;
