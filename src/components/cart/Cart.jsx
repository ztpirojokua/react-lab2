import React, { useEffect, useState } from "react";
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import CartItem from "./CartItem";
import CartTotals from "./CartTotals";

const Cart = () => {
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalQuantity, setTotalQuantity] = useState(0);

  const products = [
    { id: 1, name: "LEGO", price: 300, min: 1, max: 7, quantity: 1 },
    { id: 2, name: "Train station", price: 200, min: 1, max: 7, quantity: 3 },
    { id: 3, name: "Hot Wheels Car Pack", price: 150, min: 2, max: 12, quantity: 2 }
  ];

  useEffect(() => {
    let sumTotalQuantity = products.reduce((sum, item) => sum + item.quantity, 0);
    let sumTotalPrice = products.reduce((sum, item) => sum + item.price * item.quantity, 0);

    setTotalPrice(sumTotalPrice);
    setTotalQuantity(sumTotalQuantity);
  }, []);

  return (
    <TableContainer component={Paper}>
      <Table
        sx={{ minWidth: 650 }}
        aria-label="simple table"
      >
        <TableHead sx={{ backgroundColor: "lightyellow" }}>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Price</TableCell>
            <TableCell align="right">Quantity</TableCell>
            <TableCell align="right">Total</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {products.map(product => (
            <CartItem
              key={product.id}
              totalPrice={totalPrice}
              product={product}
              setTotalPrice={setTotalPrice}
              totalQuantity={totalQuantity}
              setTotalQuantity={setTotalQuantity}
            />
          ))}
          <CartTotals totalQuantity={totalQuantity} totalPrice={totalPrice}/>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Cart;
