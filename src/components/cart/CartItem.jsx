import React, { useState } from "react";
import { Button, TableCell, TableRow } from "@mui/material";

const CartItem = ({ totalPrice, setTotalPrice, product, totalQuantity, setTotalQuantity }) => {
  const [quantity, setQuantity] = useState(product.quantity);

  const handleOperation = (operationType) => {
    switch (operationType) {
      case "addition":
        if (quantity < product.max) {
          setQuantity(quantity + 1);
          setTotalQuantity(totalQuantity + 1);
          setTotalPrice(totalPrice + product.price);
        }
        break;
      case "removal":
        if (quantity > product.min) {
          setQuantity(quantity - 1);
          setTotalQuantity(totalQuantity - 1);
          setTotalPrice(totalPrice - product.price);
        }
        break;
      default:
        break;
    }
  };

  return (
    <TableRow
      key={product.id}
      sx={{ backgroundColor: "lightgreen" }}
    >
      <TableCell
        component="th"
        scope="row"
      >
        {product.name}
      </TableCell>
      <TableCell>{product.price}</TableCell>
      <TableCell align="right">
        <Button
          variant="text"
          onClick={() => handleOperation("removal")}
        >
          -
        </Button>
        <span>{quantity}</span>
        <Button
          variant="text"
          onClick={() => handleOperation("addition")}
        >
          +
        </Button>
      </TableCell>
      <TableCell align="right">{product.price * quantity}</TableCell>
    </TableRow>
  );
};

export default CartItem;
