import React from "react";
import { TableCell, TableRow } from "@mui/material";

const CartTotals = ({totalQuantity, totalPrice}) => {
  return (
    <TableRow
      sx={{ backgroundColor: "lightyellow" }}
    >
      <TableCell
        component="th"
        scope="row"
      >
        Totals
      </TableCell>
      <TableCell align="right"></TableCell>
      <TableCell align="right">{totalQuantity}</TableCell>
      <TableCell align="right">{totalPrice}</TableCell>
    </TableRow>
  );
};

export default CartTotals;
