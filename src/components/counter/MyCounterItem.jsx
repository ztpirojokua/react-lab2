import React, { useState } from "react";
import { Button } from "@mui/material";

const MyCounterItem = ({ minValue = -10, maxValue = 10, initialValue = 0 }) => {

  const [counter, setCounter] = useState(initialValue);

  const handleOperation = (operationType) => {
    switch (operationType) {
      case "addition":
        if (counter < maxValue) {
          setCounter(counter + 1);
        }
        break;
      case "removal":
        if (counter > minValue) {
          setCounter(counter - 1);
        }
        break;
      case "reset":
        setCounter(initialValue);
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <span>Поточний рахунок: {counter}</span>
      <Button
        variant="text"
        onClick={() => handleOperation("addition")}
      >
        +
      </Button>
      <Button
        variant="text"
        onClick={() => handleOperation("removal")}
      >
        -
      </Button>
      <Button
        variant="text"
        onClick={() => handleOperation("reset")}
      >
        Reset
      </Button>
    </div>
  );
};

export default MyCounterItem;
