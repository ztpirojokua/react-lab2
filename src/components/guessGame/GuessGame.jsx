import React, { useState } from "react";
import { Button, TextField } from "@mui/material";

const GuessGame = () => {
  const [gameStatus, setGameStatus] = useState(false);
  const [gameHints, setGameHints] = useState("");
  const [gameAttempts, setGameAttempts] = useState(0);
  const [gameField, setGameField] = useState("");
  const [gameResult, setGameResult] = useState("");
  const [gameAnswer, setGameAnswer] = useState(0);

  const handleNewGame = () => {
    clearGame();
  };

  const handleCheck = () => {
    if (!+gameField) {
      setGameField("");
      return;
    }

    if (gameAttempts + 1 === 10) {
      setGameResult("Game over!");
      setGameStatus(!gameStatus);
    }

    setGameAttempts(gameAttempts + 1);

    if (gameField === gameAnswer.toString()) {
      setGameResult("Good Job!");
      setGameStatus(!gameStatus);
      return;
    }

    let hint = (gameAnswer > gameField ? ` N > ${gameField}` : ` N < ${gameField}`) + "|";
    setGameHints(gameHints + hint);

    setGameField("");
  };

  const clearGame = () => {
    setGameStatus(!gameStatus);
    setGameHints("");
    setGameAttempts(0);
    setGameField("");
    setGameResult("");
    setGameAnswer(Math.floor(Math.random() * 1000));
  };

  return (
    <div>
      <Button
        variant="contained"
        disabled={gameStatus}
        onClick={handleNewGame}
      >
        New Game
      </Button>
      <TextField
        id="outlined-basic"
        label="Your guess"
        variant="outlined"
        value={gameField}
        onChange={(event) => setGameField(event.target.value)}
      />
      <Button
        variant="contained"
        disabled={!gameStatus}
        onClick={handleCheck}
      >
        Check
      </Button>
      <p>Info: {gameHints}</p>
      <p>Attempts: {gameAttempts}</p>
      <p>Result: {gameResult}</p>
    </div>
  );
};

export default GuessGame;
